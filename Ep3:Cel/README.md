# Ep3: Cel Shader

Shader that uses png color schemes, to create cartoon like 3D models.  

---

# YouTube Video

[![Video](https://img.youtube.com/vi/chCIyO_XUPw/0.jpg)](https://www.youtube.com/watch?v=chCIyO_XUPw)

# Resources
### Color Theory
- https://medium.com/learning-lab/how-i-learnt-about-color-theories-and-made-my-best-color-palettes-in-one-month-a461604ca669  
- https://medium.com/pixel-grimoire/how-to-start-making-pixel-art-6-a74f562a4056  
- https://youtu.be/QhgSM_tnPM4   
- Tips on hue shifting https://www.blue-canary.net/miniature-painting/painting-tips-and-guides/hue-shifting/  
### UV Unwrapping and Cel Shading
- https://youtu.be/ANBrv4isjVI

