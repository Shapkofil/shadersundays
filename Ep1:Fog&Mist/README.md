# Shader Sundays Ep 1: Fog and Mist

godot project featuring procedural mist and fog

## Youtube video:  
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/-E2FRoaT0J4/0.jpg)](https://www.youtube.com/watch?v=-E2FRoaT0J4)

## useful resources:
https://www.iquilezles.org/www/articles/fog/fog.htm  
https://docs.godotengine.org/en/stable/tutorials/shading/advanced_postprocessing.html  
https://www.shadertoy.com/view/WlSSzK  