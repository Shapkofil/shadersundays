# Ep6:Doot
Mr.Skeletal Field on shadertoy

## ShaderToy
- https://www.shadertoy.com/view/WdyyWd

## Youtube Video
https://youtu.be/_WDsGupdeL4
[![Video](https://img.youtube.com/vi/_WDsGupdeL4/0.jpg)](https://www.youtube.com/watch?v=_WDsGupdeL4)

## Desmos
- Peaks https://www.desmos.com/calculator/ghydkqzlyd
- Smoothened sawwave https://www.desmos.com/calculator/wvfwzchinz
