# Ep4:ProceduralPixel
Realtime pixel art creation using the rendering of 3D models

## Youtube Video
[![Video](https://img.youtube.com/vi/s9ZFYoj-8XQ/0.jpg)](https://www.youtube.com/watch?v=s9ZFYoj-8XQ)

## Resources
- https://youtu.be/cLnyKRfBFsQ
- https://youtu.be/DTExHmvfZhI
- https://docs.godotengine.org/en/stable/tutorials/viewports/viewports.html