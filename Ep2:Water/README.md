# Shader Sundays Ep 2: WATER

Ocean shader heavily inspired by Sebastian Lague.  
The shader is build for godot, but in theory could be applied to any piece of software.

## Youtube video:  
[![Video](https://img.youtube.com/vi/_veqnRfYcp8/0.jpg)](https://www.youtube.com/watch?v=_veqnRfYcp8)

## useful resources:
https://www.iquilezles.org/www/articles/intersectors/intersectors.htm  
Sphere Ocean: 
https://github.com/SebLague/Solar-System/blob/Development/Assets/Scripts/Celestial/Shaders/PostProcessing/OceanEffect.shader

